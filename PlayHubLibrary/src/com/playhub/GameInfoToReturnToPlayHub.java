package com.playhub;


import java.io.Serializable;
import java.util.Map;

/**
 * The information to return to PlayHub at the end of the move.
 * This information needs to be populated by the game activity and then returned to PlayHub.
 * 
 * Note that if your game cannot start because there are not enough players (for example you require
 * a minimum of 3 players and there currently are 1 or 2) you should inform the user and return
 * no data to PlayHub. Returning no data tells PlayHub nothing happened. In PlayHub, more users can
 * still join the game as long as it hasn't started. So maybe in the next time the game activity is called
 * there will be enough users. However, if there is another problem with the game,
 * such as too many players, you should return one of the designated GameStateToReturnToPlayhub options that
 * will finish the game. This is because unlike the problem of not enough players, these problems
 * do not have the possibility of changing in the future.
 * 
 * Should be sent from the game activity as follows:
 * 
 * <blockquote>
 * <pre>
 * {@code
 * output.putExtra(Consts.GAME_INFO_TO_RETURN_TO_PLAYHUB_KEY, gameInfoToReturnToPlayHub);
 * setResult(RESULT_OK, output);
 * finish();
 * }
 * </pre>
 * </blockquote>
 * 
 * @author alon
 */
public class GameInfoToReturnToPlayHub implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/**
	 * The game's current state. This is returned to PlayHub as part of the GameInfoToReturnToPlayHub.
	 * 
	 * @author alon
	 */
	public enum GameStateToReturnToPlayhub {
		/**
		 * The game is currently running.
		 */
		RUNNING, 
		
		/**
		 * The game has finished in a regular manner (i.e. someone won, or some other scenario legitimate for your game).
		 */
		FINISHED,
		
		/**
		 * The game has been cancelled by the user in some way. Irrelevant for most games, but you can wish this state if you please.
		 * It only differs to the users from FINISHED in the message they receive about the ending of the game, and that is doesn't
		 * require the game to return who is the "winner" (as their obviously isn't one).
		 */
		CANCELLED_BY_USER,
		
		/**
		 * The game had to be stopped by the game itself. Irrelevant for most games, but you can wish this state if you please.
		 * It only differs to the users from FINISHED and CANCELLED_BY_USER in the message they receive about the ending of the game, and
		 * as CANCELLED_BY_USER, if this is returned then you are not required to retuen a valid winnerUserId, as their is no "winner".
		 */
		CANCELLED_BY_GAME
	}

	/**
	 * The index in the users array of the player which turn it now is. If the returned gameState is RUNNING, this member must be a valid
	 * userId of one of the game's users.
	 */
	public int currentTurnPlayerUserIndex;
	
	/**
	 * The index in the users array of the player who won. If the game state does not indicates the game was won, this value is ignored.
	 */
	public int winnerUserIndex;
	
	/**
	 * The game's current state, after the current move (i.e. now).
	 * See GameStateToReturnToPlayhub documentation for more info.
	 */
	public GameStateToReturnToPlayhub gameState;
	
	/**
	 * The game's inner state. This buffer will be passed AS IS to the game as part of GameInfoFromPlayHub next time it is
	 * called for this specific game. See GameInfoFromPlayHub documentation for more info.
	 * 
	 * Can be at most 512*1024 (512KB) long. Any bigger and the play will be discarded.
	 */
	public byte[] innerGameState;
	
	/**
	 * This is an OPTIONAL MEMBER, and can be left as null if not needed. This allows you to create
	 * specific messages that will be sent to specific users instead of the default PlayHub messages.
	 * 
	 * For example, if the user Johnnie (thats his nickname) played a move, and it is now Bradley's turn, PlayHub will send
	 * a notification to all the other users with something like "Johnny played in the game, it is Bradley's turn now".
	 * If you wish for another message to be sent to any or all of the users, for example, sent to Bradley something like
	 * "Oh no, Johnny just killed all your sheep!", simply put the message in this map, with bradley's index in the users array as the key.
	 * 
	 * PlayHub uses this messages instead of the default messages for each user whose index in the users array appears in the map.
	 */
	public Map<Integer, String> perUserMessages;
}
