package com.playhub;


import java.io.Serializable;

/**
 * The user information received from PlayHub about a single user in the game.
 * 
 * @author alon
 *
 */
public class UserInfoFromPlayHub implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/**
	 * The user's unique id. Specifically, it is the user's email, and it cannot be null.
	 */
	public String userId;
	
	/**
	 * The user's nickname. Might be the user's email (i.e. identical to the user's id), but this only happens
	 * if the user hasn't configured a nickname in PlayHub. Anyway, it is not something to worry about.
	 */
	public String nickname;
	
	/**
	 * The user's image uri. This might be null, if the user hadn't configured for himself an image
	 * in PlayHub. If it is not null, it is guaranteed to be a square image with size at least 256 on 256 pixels.
	 * In order to read it, your application must have permissions to read from external storage.
	 * 
	 * Example of using this to load the user's image into an ImageView:
	 * 
	 *	if (player.imageUri != null){
	 *		Uri imageUri = Uri.parse(player.imageUri);
	 *		imageView.setImageURI(imageUri);
	 *	}
	 */
	public String imageUri;
}
