package com.playhub;

import java.io.Serializable;

/**
 * The entire information received by the game activity from PlayHub. Contains the game's inner state as previously serialized by the game, along
 * with metadata about the game, such as the game's current player, the game's users, etc.
 * 
 * Should be read in the game's activity as follows:
 * 
 * <blockquote>
 * <pre>
 * {@code
 * 
 * protected void onCreate(Bundle savedInstanceState) {
 *		...
 *		Bundle extras = getIntent().getExtras();
 *		if ((extras != null) && (extras.containsKey(Consts.GAME_INFO_FROM_PLAYHUB_KEY))){
 *			gameInfoFromPlayHub = (GameInfoFromPlayHub) extras.getSerializable(Consts.GAME_INFO_FROM_PLAYHUB_KEY);
 *		}
 * }
 * }
 * </pre>
 * </blockquote>
 * 
 * @author alon
 *
 */
public class GameInfoFromPlayHub implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/**
	 * An array containing information about all the users in the game.
	 * Once the game started (The first move has been played) you are guaranteed
	 * that the list of users will not change in future calls. In PlayHub, a user
	 * cannot join a game that has already started, and if a user leaves a game, the
	 * game is automatically cancelled.
	 */
	public UserInfoFromPlayHub[] users;
	
	/**
	 * The index in the users array of the player whose turn it currently is. When a game is first created, the first player is
	 * ALWAYS the creating player, so that is the player's user id which PlayHub will pass to the game activity
	 * upon the game's first move.
	 */
	public int currentTurnUserIndex;
	
	/**
	 * The index in the users array of the player who is currently viewing the game.
	 */
	public int viewingUserIndex;
	
	/**
	 * The game-specific data. This buffer is received from the game and sent AS IS back to the game. The
	 * game should store in it (and serialize to a byte array however it sees fit) ALL the information needed to pass
	 * the game's state from one player to another, excluding the little data which is passed in the game's metatata (such
	 * as users list, or who is the current player).
	 * 
	 * If this is the first time PlayHub calls the game's activity for this specific game, this member will be null (which
	 * is reasonable, as PlayHub cannot by itself initialize this game-specific data).
	 */
	public byte[] innerGameState;
	
	/**
	 * Whether the game has finished or not. The game may have finished for many reasons, either
	 * because the one of the players won, your game activity previously decided to cancel the game, or if one of
	 * the users cancelled the game via PlayHub.
	 * 
	 * If this is true, there is no importance to the data you return to PlayHub in GameInfoToReturnToPlayHub
	 * (you don't even need to return it), as PlayHub will ignore it since the game is already over.
	 */
	public boolean isGameAlreadyFinished;
}
