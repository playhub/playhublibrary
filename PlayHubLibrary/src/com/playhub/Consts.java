package com.playhub;

/**
 * Constants used for passing PlayHub's information to and from the game activity.
 * 
 * @author alon
 *
 */
public class Consts {
	/**
	 * The key with which the game's information is stored in the intent's extras with which the game activity was called.
	 */
	public static final String GAME_INFO_FROM_PLAYHUB_KEY = "GAME_INFO_FROM_PLAYHUB_KEY";
	
	/**
	 * The key with which the game's information is stored in the intent's extras with which the game activity has returned.
	 */
	public static final String GAME_INFO_TO_RETURN_TO_PLAYHUB_KEY = "GAME_INFO_TO_RETURN_TO_PLAYHUB_KEY";
}
